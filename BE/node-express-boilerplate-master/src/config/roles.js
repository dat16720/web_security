const allRoles = {
  user: [],
  employee: ['createSurvey'],
  admin: ['getUsers', 'manageUsers', 'getSurveys', 'updateSurvey', 'createSurvey'],
};

const roles = Object.keys(allRoles);
const roleRights = new Map(Object.entries(allRoles));

module.exports = {
  roles,
  roleRights,
};
