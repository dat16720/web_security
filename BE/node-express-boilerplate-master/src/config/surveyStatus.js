const surveyStatus = {
    WAITING_TO_REVIEW: 'WAITING_TO_REVIEW', // chờ được phê duyệt
    APPROVED: 'APPROVED', // đã được phê duyệt
    REJECTED: 'REJECTED', // đã bị từ chối
    BLOCK: 'BLOCK', // bị kho
  };
  
  module.exports = {
    surveyStatus,
  };
  