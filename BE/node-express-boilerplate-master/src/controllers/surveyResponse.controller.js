import { catchAsync } from '../utils/catchAsync';
import { surveyResponseService } from '../services';
import ApiError from '../utils/ApiError';
import httpStatus from 'http-status';
import { SurveyModel, SurveyResponseModel } from '../models';
import { pick } from '../utils/pick';
import { responseHelper } from '../helpers/responseHelper';
//function to create surveyResponse
const createSurveyResponse = catchAsync(async (req, res, next) => {
  const survey = await SurveyModel.findOne(
    {
      _id: req.body.surveyId,
      deletedById: { $exists: false }
    }
  );
  if (!survey) {
    return next(new ApiError(httpStatus.NOT_FOUND, 'survey.not_found'));
  }
  if (survey.surveyEndDate < new Date()) {
    return next(new ApiError(httpStatus.NOT_FOUND, 'survey.expired'));
  }
  const surveyResponse = await surveyResponseService.createSurveyResponse(req.body);
  responseHelper.successResponse({ req, res, data: surveyResponse, status_code: 200 })
});
//function to update surveyResponse
const update = catchAsync(async (req, res, next) => {
  const data = await surveyResponseService.updateSurveyResponse(req.params.surveyResponseId, req.body);
  if (!data) {
    return next(new ApiError(httpStatus.NOT_FOUND, 'surveyResponse.not_found'));
  }
  res.send(data)
});
//function to get list surveyResponse
const getSurveyResponseList = catchAsync(async (req, res) => {
  const filters = pick(req.query, ['status', 'createdById', 'surveyId']); //TODO: add more filters
  const options = pick(req.query, ['limit', 'page', 'sort']);
  const data = await surveyResponseService.getSurveyResponseList(filters, options);
  res.send(data)
});
//function to delete surveyResponse
const deleteById = catchAsync(async (req, res, next) => {
  const data = await surveyResponseService.deleteSurveyResponseById(req.params.surveyResponseId, req.body);
  if (!data) {
    return next(new ApiError(httpStatus.NOT_FOUND, 'surveyResponse.not_found'));
  }
  res.send(data)
});
//function to get data of a surveyResponse
const getSurveyResponseById = catchAsync(async (req, res, next) => {
  const data = await surveyResponseService.getSurveyResponseById(req.params.surveyResponseId);
  if (!data) {
    return next(new ApiError(httpStatus.NOT_FOUND, 'surveyResponse.not_found'));
  }
  res.send(data)
});

export const surveyResponseController = {
  getSurveyResponseList,
  update,
  deleteById,
  createSurveyResponse,
  getSurveyResponseById,
  
}

