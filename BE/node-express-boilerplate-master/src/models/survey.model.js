const mongoose = require('mongoose');

const { surveyStatus } = require('../config/surveyStatus');
const { toJSON, paginate } = require('./plugins');
const SurveySchema = mongoose.Schema(
  {
    title: {
      type: String,
    },
    introduction: {
      type: String,
    },
    description: {
      type: String,
    },
    questions: {
      type: [
        {
          question: {
            type: String,
          },
        },
      ],
    },
    surveyStartDate: {
      type: Date,
    },
    surveyEndDate: {
      type: Date,
    },
    status: {
      type: String,
      enum: surveyStatus,
      default: surveyStatus.WAITING_TO_REVIEW
    },
    createdById: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: false,
    },
    updatedById: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: false,
    },
    deletedById: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: false,
    },
    deletedAt: { type: Date, required: false },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    }
  }
);

// add plugin that converts mongoose to json
// add plugin that converts mongoose to json
SurveySchema.plugin(toJSON);
SurveySchema.plugin(paginate);
/**
 * @typedef Survey
 */
const Survey = mongoose.model('Survey', SurveySchema);

module.exports = Survey;
