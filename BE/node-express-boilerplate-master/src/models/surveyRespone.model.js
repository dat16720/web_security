const mongoose = require('mongoose');

// const { surveyStatus } = require('../config/surveyStatus');
const { toJSON, paginate } = require('./plugins');
const surveyResponseModelSchema = mongoose.Schema(
  {
    surveyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Survey',
        required: true
    },
    questionResponses: {
        type: [{
          questionId: {
            type: String,
          },
          question: {
            type: String,
          },
          response: {
            type: String,
          },
        }]
      },
      createdById: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
      },
      updatedById: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
      },
      deletedById: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false
      },
      deletedAt: { type: Date, required: false },
    
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    }
  }
);

// add plugin that converts mongoose to json
// add plugin that converts mongoose to json
surveyResponseModelSchema.plugin(toJSON);
surveyResponseModelSchema.plugin(paginate);
/**
 * @typedef SurveyRespone
 */
const SurveyRespone = mongoose.model('SurveyRespone', surveyResponseModelSchema);

module.exports = SurveyRespone;
