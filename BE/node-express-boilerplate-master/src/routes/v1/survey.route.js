const express = require('express');
const validate = require('../../middlewares/validate');
const auth = require('../../middlewares/auth');
const surveyController = require('../../controllers/survey.controller');
const surveyValidation = require('../../validations/survey.validation');
// const auth = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/create-survey')
  .post(auth('createSurvey'), validate(surveyValidation.create), surveyController.createSurvey)
  .get(auth(), validate(surveyValidation.getSurveys), surveyController.getSurveys);

router
  .route('/:surveyId')
  .get(auth(), validate(surveyValidation.getSurvey), surveyController.getSurvey)
  .delete(auth('updateSurvey'), validate(surveyValidation.deleteSurvey), surveyController.deleteSurvey)
  .patch(auth('updateSurvey'), validate(surveyValidation.updateSurvey), surveyController.updateSurvey);
// router
//   .route('/:surveyId')
//   .post(auth(), validate(surveyValidation.deleteSurvey), surveyController.deleteSurvey)



// .post(auth('manageUsers'), validate(userValidation.createUser), userController.createUser)
//   .get(auth('getUsers'), validate(userValidation.getUsers), userController.getUsers);

module.exports = router;
