const httpStatus = require('http-status');
// const { User } = require('../models');
const { Survey } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createSurvey = async (userBody) => {
  //   if (await User.isEmailTaken(userBody.email)) {
  //     throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  //   }
  return Survey.create(userBody);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const querySurveys = async (filter, options) => {
  const surveys = await Survey.paginate(filter, options);
  return surveys;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getSurveyById = async (id) => {
  return Survey.findById(id);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} surveyId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateSurveyById = async (surveyId, updateBody) => {
  const survey = await getSurveyById(surveyId);
  if (!survey) {
    throw new ApiError(httpStatus.NOT_FOUND, 'survey not found');
  }
  // if (updateBody.email && (await survey.isEmailTaken(updateBody.email, surveyId))) {
  //   throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  // }
  Object.assign(survey, updateBody);
  await survey.save();
  return survey;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteSurveyById = async (surveyId) => {
  const survey = await getSurveyById(surveyId);
  if (!survey) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Survey not found');
  }
  await survey.remove();
  return survey;
};

module.exports = {
  createSurvey,
  querySurveys,
  getSurveyById,
  updateSurveyById,
  //   getUserByEmail,
  //   updateUserById,
  deleteSurveyById,
};
