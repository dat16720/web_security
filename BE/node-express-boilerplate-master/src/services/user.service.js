const httpStatus = require('http-status');
const { User } = require('../models');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const pbkdf2 = require('pbkdf2');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  return User.create(userBody);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (filter, options) => {
  const users = await User.paginate(filter, options);
  return users;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (id) => {
  return User.findById(id);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.remove();
  return user;
};

const updatePassword = catchAsync(async (req, res, next) => {
  if (req.body.newPassword == req.body.oldPassword) {
    return next(new ApiError(httpStatus.NOT_FOUND, 'user.new_password_old_password_can_not_be_the_same'));
  }
  const user = await User.findOne({ _id: req.params.userId });
  if (!user) {
    return next(new ApiError(httpStatus.NOT_FOUND, 'user.not_found'));
  } else {
    if (await user.isPasswordMatch(req.body.oldPassword) === true) {
      // const newPassword = pbkdf2.pbkdf2Sync(req.body.newPassword, process.env.SALT, 100000, 64, 'sha256').toString('hex');
      const data = await updateUserById(req.params.userId, { password: req.body.newPassword });
      if (!data) {
        return next(new ApiError(httpStatus.NOT_FOUND, 'user.not_found'));
      }
      res.send(data);
    } else {
      return next(new ApiError(httpStatus.NOT_FOUND, 'user.wrong_old_password'));
    }
  }
});
module.exports = {
  createUser,
  queryUsers,
  getUserById,
  getUserByEmail,
  updateUserById,
  deleteUserById,
  updatePassword,
};
