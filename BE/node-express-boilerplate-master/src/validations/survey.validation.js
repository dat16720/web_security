const Joi = require('joi')
const { updateEntityValidation, createEntityValidationWhenFind, createEntityValidation } = require('./custom.validation');
const {objectId} = require('./custom.validation')
const create = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
    questions: Joi.array()
      .items(
        Joi.object().keys({
          question: Joi.string().required(),
        })
      )
      .max(20)
      .required(),
    surveyStartDate: Joi.date(), //required
    surveyEndDate: Joi.date(), //required
    ...createEntityValidation,
  }),
};

const getSurveys = {
  query: Joi.object().keys({
    title: Joi.string(),
    description: Joi.string(),
    status: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getSurvey = {
  params: Joi.object().keys({
    surveyId: Joi.string().custom(objectId),
  }),
};
const deleteSurvey = {
  params: Joi.object().keys({
    surveyId: Joi.string().custom(objectId),
  }),
};
const updateSurvey = {
  params: Joi.object().keys({
    surveyId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      status: Joi.string().required(),
      // email: Joi.string().email(),
      // password: Joi.string().custom(password),
      // name: Joi.string(),
    })
};
module.exports = {
  create,
  getSurveys,
  getSurvey,
  deleteSurvey,
  updateSurvey
};
