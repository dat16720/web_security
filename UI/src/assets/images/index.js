const images = {
  logo: require('~/assets/images/logo.svg').default,
  noImage: require('~/assets/images/no-image.png'),
  loginBackground: require('~/assets/images/login-background.jpg'),
};

export default images;
