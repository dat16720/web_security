import classNames from 'classnames/bind';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

import Button from '~/components/Button';
import styles from './AccountPreview.module.scss';

const cx = classNames.bind(styles);

function AccountPreview({userInfo}) {
  return (
    <div className={cx('wrapper')}>
      <div className={cx('header')}>
        <img
          className={cx('avatar')}
          src="https://scontent.fhan15-2.fna.fbcdn.net/v/t39.30808-6/309496096_772341970522768_6756410377838988840_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=h5RtOVrlhq8AX9hRZnO&_nc_ht=scontent.fhan15-2.fna&oh=00_AT-bWJ3B3GkiN1aTAbJ9igI9ryZ5Lua3VOUFMW2c9F8VrA&oe=634162A2"
          alt=""
        />
        <Button className={cx('follow-btn')} primary>
          Follow
        </Button>
      </div>
      <div className={cx('body')}>
        <p className={cx('nickname')}>
        <strong>{userInfo?.name}</strong>
          <FontAwesomeIcon className={cx('check')} icon={faCheckCircle} />
        </p>
        <p className={cx('name')}>{userInfo?.name} -{userInfo?.role} - AT150512</p>
        <p className={cx('analytics')}>
          <strong className={cx('value')}>8.2M</strong>
          <span className={cx('label')}>Followers</span>
          <strong className={cx('value')}>8.2M </strong>
          <span className={cx('label')}>Likes</span>
        </p>
      </div>
    </div>
  );
}

export default AccountPreview;
