const routes = {
  home: '/',

  profile: '/@:nickname',
  upload: '/upload',
  search: '/search',
  live: '/live',
  login: '/login',
  register: '/register',
  forgotpass: '/forgot-password',
  resetpass: '/reset-password/:token',
  ///
  createSurvey: 'survey/create',
  survey: '/survey',
  surveyId: '/survey/:surveyId',
};

export default routes;
