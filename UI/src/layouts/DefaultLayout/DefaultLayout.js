import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import Header from '~/layouts/components/Header';
import Sidebar from '~/layouts/components/Sidebar';
import styles from './DefaultLayout.module.scss';
import Footer from '~/layouts/components/Footer';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
const cx = classNames.bind(styles);

function DefaultLayout({ children }) {
  const userToken = localStorage.getItem('token');
  const navigate = useNavigate();
  useEffect(() => {
    if (!userToken) {
      navigate('/login');
    }
  }, [userToken]);

  return (
    <div className={cx('wrapper')}>
      <Header />
      <div className={cx('container')}>
        <Sidebar />
        <div className={cx('content')}>{children}</div>
      </div>
      <Footer />
    </div>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default DefaultLayout;
