import React from 'react';
import classNames from 'classnames/bind';
import styles from './Footer.module.scss';
import { Col, Divider, Row } from 'antd';
import { InstagramOutlined, FacebookOutlined, YoutubeOutlined, TwitterOutlined, HeartTwoTone } from '@ant-design/icons';
const cx = classNames.bind(styles);
const Footer = () => {
  return (
    <div className={cx('wrapper')}>
      <Row>
        <Col span={8}>
          <div className={cx('title_contact')}>Thông tin bản quyền</div>
        </Col>
        <Col span={8} offset={8}>
          <Row>
            <Col span={16}>
              <div className={cx('title_contact')}>More about me</div>
              <div>
                <div className={cx('list_fltitle')}> Info</div>
                <div className={cx('list_fltitle')}> Da dẻ</div>
              </div>
            </Col>
            <Col span={8}>
              <div className={cx('title_contact')}>Contact me</div>
              <div>
                <div className={cx('list_fltitle')}> Quang cao</div>
                <div className={cx('list_fltitle')}> Mon hoc</div>
              </div>
            </Col>
          </Row>
          <Divider style={{ backgroundColor: '#fff' }} />
          <Row style={{ justifyContent: 'space-around' }}>
            <InstagramOutlined style={{ fontSize: '32px', color: '#fff' }} />
            <FacebookOutlined style={{ fontSize: '32px', color: '#fff' }} />
            <YoutubeOutlined style={{ fontSize: '32px', color: '#fff' }} />
            <TwitterOutlined style={{ fontSize: '32px', color: '#fff' }} />
          </Row>
        </Col>
      </Row>
      <Divider style={{ backgroundColor: '#fff' }} />
      <Row>
        <div className={cx('text_last')}>
          Create by Trọng Đạt <HeartTwoTone twoToneColor="#fe2c55" /> with love
        </div>
      </Row>
    </div>
  );
};

export default Footer;
