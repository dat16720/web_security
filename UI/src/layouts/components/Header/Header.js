import classNames from 'classnames/bind';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCircleQuestion,
  faCoins,
  faEarthAsia,
  faEllipsisVertical,
  faGear,
  faKeyboard,
  faSignOut,
  faUser,
} from '@fortawesome/free-solid-svg-icons';
import { Link, useNavigate } from 'react-router-dom';
import Tippy from '@tippyjs/react';
import 'tippy.js/dist/tippy.css';

import config from '~/config';
import Button from '~/components/Button';
import styles from './Header.module.scss';
import images from '~/assets/images';
import Menu from '~/components/Popper/Menu';
import { InboxIcon, MessageIcon, UploadIcon } from '~/components/Icons';
import Image from '~/components/Image';
import Search from '../Search';
import { PlusCircleOutlined } from '@ant-design/icons';
import { LogOutUser } from '~/services/user.service';
import { notification } from 'antd';
const cx = classNames.bind(styles);

const MENU_ITEMS = [
  {
    icon: <FontAwesomeIcon icon={faEarthAsia} />,
    title: 'English',
    children: {
      title: 'Language',
      data: [
        {
          type: 'language',
          code: 'en',
          title: 'English',
        },
        {
          type: 'language',
          code: 'vi',
          title: 'Tiếng Việt',
        },
      ],
    },
  },
  {
    icon: <FontAwesomeIcon icon={faCircleQuestion} />,
    title: 'Feedback and help',
    to: '/feedback',
  },
  {
    icon: <FontAwesomeIcon icon={faKeyboard} />,
    title: 'Keyboard shortcuts',
  },
];

function Header() {
    let navigate = useNavigate();
  const currentUser = true;
  let refreshToken = localStorage.getItem('refreshToken');
  //HANDEL LOGOUT
  const handleLogout = async () => {
    
    let res = await LogOutUser({refreshToken});
    console.log('ress', res);
    if (res?.status === 204) {
      localStorage.clear();
      navigate('/login');
    } else {
      notification['error']({ message: 'Log out thất bại' });
      navigate("/");
    }
  };
  // Handle logic
  const handleMenuChange = (menuItem) => {
    switch (menuItem.type) {
      case 'language':
        // Handle change language
        break;
      case 'logout':
        handleLogout();
        break;
      default:
    }
  };

  const userMenu = [
    {
      icon: <FontAwesomeIcon icon={faUser} />,
      title: 'View profile',
      to: '/@my-info',
    },
    // {
    //     icon: <FontAwesomeIcon icon={faCoins} />,
    //     title: 'Get coins',
    //     to: '/coin',
    // },
    {
      icon: <FontAwesomeIcon icon={faGear} />,
      title: 'Settings',
      to: '/settings',
    },
    // ...MENU_ITEMS,
    {
      icon: <FontAwesomeIcon icon={faSignOut} />,
      title: 'Log out',
      // to: '/logout',
      type: 'logout',
      separate: true,
    },
  ];

  return (
    <header className={cx('wrapper')}>
      <div className={cx('inner')}>
        <Link to={config.routes.home} className={cx('logo-link')}>
          Web An toàn
          {/* <img src={images.logo} alt="datdt" /> */}
        </Link>

        <Search />

        <div className={cx('actions')}>
          {currentUser ? (
            <>
              <Tippy delay={[0, 50]} content="Tạo khảo sát" placement="bottom">
                <Link to="/survey/create" className={cx('action-btn')}>
                  <PlusCircleOutlined style={{ fontSize: '32px', color: '#08c' }} />
                </Link>
              </Tippy>
              <Tippy delay={[0, 50]} content="Message" placement="bottom">
                <button className={cx('action-btn')}>
                  <MessageIcon />
                </button>
              </Tippy>
              <Tippy delay={[0, 50]} content="Inbox" placement="bottom">
                <button className={cx('action-btn')}>
                  <InboxIcon />
                  <span className={cx('badge')}>12</span>
                </button>
              </Tippy>
            </>
          ) : (
            <>
              <Button text>Upload</Button>
              <Button primary>Log in</Button>
            </>
          )}

          <Menu items={currentUser ? userMenu : MENU_ITEMS} onChange={handleMenuChange}>
            {currentUser ? (
              <Image
                className={cx('user-avatar')}
                src="https://scontent.fhan2-1.fna.fbcdn.net/v/t39.30808-6/262356627_590030998753867_1065319490578620053_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=174925&_nc_ohc=7ZR739vBVisAX8D3mqM&_nc_ht=scontent.fhan2-1.fna&oh=00_AT_ph9yJGPHXK-PqqaiOEKn9bsOT6rZ473UES9X1Ur8Dhw&oe=6328E02B"
                alt="Nguyen Van A"
              />
            ) : (
              <button className={cx('more-btn')}>
                <FontAwesomeIcon icon={faEllipsisVertical} />
              </button>
            )}
          </Menu>
        </div>
      </div>
    </header>
  );
}

export default Header;
