import classNames from 'classnames/bind';
import styles from './Sidebar.module.scss';
import Menu, { MenuItem } from './Menu';
import {
  HomeIcon,
  HomeActiveIcon,
  UserGroupIcon,
  UserGroupActiveIcon,
  LiveIcon,
  LiveActiveIcon,
} from '~/components/Icons';
import SuggestedAccounts from '~/components/SuggestedAccounts';
import config from '~/config';
import { Avatar } from 'antd';
import imgAvatar from '~/assets/images/avtar_profile.jpg';
const cx = classNames.bind(styles);

function Sidebar() {
  return (
    <aside className={cx('wrapper')}>
      <div className={cx('avatar_profile')}>
        <div className={cx('avatar_des')}>
          <Avatar size={190} src={imgAvatar} />
        </div>
      </div>
      <Menu>
        <MenuItem title="Trang chủ" to={config.routes.home} icon={<HomeIcon />} activeIcon={<HomeActiveIcon />} />
        <MenuItem
          title="Khảo sát"
          to={config.routes.survey}
          icon={<UserGroupIcon />}
          activeIcon={<UserGroupActiveIcon />}
        />
        {/* <MenuItem title="LIVE" to={config.routes.live} icon={<LiveIcon />} activeIcon={<LiveActiveIcon />} /> */}
      </Menu>

      <SuggestedAccounts label="Thông tin cá nhân" />
    </aside>
  );
}

export default Sidebar;
