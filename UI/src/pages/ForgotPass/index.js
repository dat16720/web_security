import React from 'react';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input } from 'antd';
import styles from './ForgotPass.module.scss';
import classNames from 'classnames/bind';
import images from '~/assets/images/index';
import { Link } from 'react-router-dom';
import { forgotPass } from '~/services/user.service';
import notificationToast from '~/utils/toast';

const cx = classNames.bind(styles);

const ForgotPassword = () => {
  const onFinish = async (values) => {
    let resData = await forgotPass(values)
    notificationToast('success', 'Check mail và làm theo hướng dẫn')

  };

  return (
    <div className={cx('wrapper')} style={{ backgroundImage: `url(${images.loginBackground})` }}>
      <div className={cx('form-wrapper')}>
        <div className={cx('login-title')}>Quên mật khẩu</div>
        <Form name="normal_login" className={cx('login-form')} onFinish={onFinish}>
          <Form.Item>
            <div className={cx('login-form-register-text')}>
              Nhập địa chỉ email bạn đã sử dụng khi tham gia và chúng tôi sẽ gửi cho bạn hướng dẫn để đặt lại mật khẩu
              của bạn.
            </div>
            <div className={cx('login-form-register-text')}>
              Vì lý do bảo mật, chúng tôi KHÔNG lưu trữ mật khẩu của bạn. Vì vậy, hãy yên tâm rằng chúng tôi sẽ không
              bao giờ gửi mật khẩu của bạn qua email.
            </div>
            <div className={cx('login-title')}>Nhập địa chỉ e-mail</div>
          </Form.Item>

          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập Email của bạn!',
              },
            ]}
          >
            <Input prefix={<UserOutlined className={cx('site-form-item-icon')} />} placeholder="Nhập email của bạn" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" className={cx('login-form-button')}>
              Gửi hướng dẫn về Email
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default ForgotPassword;
