import classNames from 'classnames/bind';
import styles from './Home.module.scss';
import { Avatar, Button, List, Skeleton, Col, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import { deteleUser, getUsers } from '~/services/user.service';
import { Link } from 'react-router-dom';
import avataDefault from '~/assets/images/index';
import { DeleteOutlined } from '@ant-design/icons';
const count = 3;
const fakeDataUrl = `https://randomuser.me/api/?results=${count}&inc=name,gender,email,nat,picture&noinfo`;
const cx = classNames.bind(styles);
const Home = () => {
  const [initLoading, setInitLoading] = useState(true);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [list, setList] = useState([]);
  const [listEmployee, setListEmployees] = useState([]);
  const [listUser, setListUsers] = useState([]);
  const userInfo = JSON.parse(localStorage.getItem('userInfo'));
  // const getListEmployeeAndUser = async () => {
  //   let resData = await getUsers();

  //   let tempArr = resData.results;
  //   setListEmployees(tempArr.filter((item) => item.role === 'employee'));
  //   setListUsers(tempArr.filter((item) => item.role === 'user'));
  //   setInitLoading(false);
  // };
  console.log("first",listEmployee )
  const handleDelete = async (userId) => {
    let resData = await deteleUser(userId);

    setLoading(true);
  };
  const [pramsEmployee, setPramsEmployee] = useState({
    role: 'employee',
  });
  const [pramsUser, setPramsUser] = useState({
    role: 'user',
  });
  const getListEmployee = async () => {
    let resData = await getUsers(pramsEmployee);
    setListEmployees(resData.results);

    setInitLoading(false);
  };
  const getListUser = async () => {
    let resData = await getUsers(pramsUser);
    setListUsers(resData.results);

    setInitLoading(false);
  };
  useEffect(() => {
    // getListEmployeeAndUser();
    getListEmployee();
    getListUser();
  }, [loading]);

  // useEffect(() => {
  //   fetch(fakeDataUrl)
  //     .then((res) => res.json())
  //     .then((res) => {
  //       setInitLoading(false);
  //       setData(res.results);
  //       setList(res.results);
  //     });
  // }, []);

  const onLoadMore = () => {
    setLoading(true);
    setList(
      data.concat(
        [...new Array(count)].map(() => ({
          loading: true,
          name: {},
          picture: {},
        })),
      ),
    );
    fetch(fakeDataUrl)
      .then((res) => res.json())
      .then((res) => {
        const newData = data.concat(res.results);
        setData(newData);
        setList(newData);
        setLoading(false); // Resetting window's offsetTop so as to display react-virtualized demo underfloor.
        // In real scene, you can using public method of react-virtualized:
        // https://stackoverflow.com/questions/46700726/how-to-use-public-method-updateposition-of-react-virtualized

        window.dispatchEvent(new Event('resize'));
      });
  };

  const loadMore =
    !initLoading && !loading ? (
      <div
        style={{
          textAlign: 'center',
          marginTop: 12,
          height: 32,
          lineHeight: '32px',
        }}
      >
        <Button onClick={onLoadMore}>loading more</Button>
      </div>
    ) : null;
  return (
    <div className={cx('wrapper')}>
      {userInfo?.role === 'admin' ? (
        <Row>
          <Col span={12}>
            <h2>Danh sách nhân viên</h2>
            <List
              className="demo-loadmore-list"
              loading={initLoading}
              itemLayout="horizontal"
              loadMore={loadMore}
              dataSource={listEmployee}
              renderItem={(item) => (
                
                <List.Item
                  actions={[
                    <a key="list-loadmore-edit" onClick={() => handleDelete(item.id)}>
                      <DeleteOutlined style={{ color: 'red' }} />
                    </a>,
                    <a key="list-loadmore-more"></a>,
                  ]}
                >
                  <Skeleton avatar title={false} loading={item.loading} active>
                    <List.Item.Meta
                      avatar={<Avatar src={item?.picture || avataDefault.loginBackground} />}
                      title={<Link to={`user/${item.id}`}>{item.name}</Link>}
                      description={
                        <div>
                          <div>Email: {item.email}</div> <div>số điện thoại: {item.phoneNumber}</div>
                        </div>
                      }
                    />
                    {/* <div>content</div> */}
                  </Skeleton>
                </List.Item>
              )}
            />
          </Col>
          <Col span={12}>
            <h2>Danh sách khách hàng</h2>
            <List
              className="demo-loadmore-list"
              loading={initLoading}
              itemLayout="horizontal"
              loadMore={loadMore}
              dataSource={listUser}
              renderItem={(item) => (
                <List.Item
                  actions={[
                    <a key="list-loadmore-edit" onClick={() => handleDelete(item.id)}>
                      <DeleteOutlined style={{ color: 'red' }} />
                    </a>,
                    <a key="list-loadmore-more"></a>,
                  ]}
                >
                  <Skeleton avatar title={false} loading={item.loading} active>
                    <List.Item.Meta
                      avatar={<Avatar src={item?.picture || avataDefault.loginBackground} />}
                      title={<Link to={`user/${item.id}`}>{item.name}</Link>}
                      description={
                        <div>
                          <div>Email: {item.email}</div> <div>số điện thoại: {item.phoneNumber}</div>
                        </div>
                      }
                    />
                  </Skeleton>
                </List.Item>
              )}
            />
          </Col>
        </Row>
      ) : (
        'Không có gì'
      )}
    </div>
  );
};

export default Home;
