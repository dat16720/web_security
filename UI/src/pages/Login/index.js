import React, { useState, useEffect, useRef } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input } from 'antd';
import styles from './Login.module.scss';
import classNames from 'classnames/bind';
import images from '~/assets/images/index';
import { Link } from 'react-router-dom';
import { loginNormal } from '~/services/user.service';
import { useNavigate } from 'react-router-dom';
import notificationToast from '~/utils/toast';
const cx = classNames.bind(styles);

const LoginPage = () => {
  const navigate = useNavigate();
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const reRef = useRef();
  const user = localStorage.getItem('token');
  // useEffect(() => {
  //   reRef.current.reset();
  //   //  window.location.reload(true)
  // }, [user]);
  const onFinish = async (values) => {
    console.log('Received values of form: ', values);
    const dataBody = {
      email: values.email,
      password: values.password,
    };
    try {
      const res = await loginNormal(dataBody);
      console.log('user', res);
      if (res) {
        localStorage.setItem('token', res.tokens.access.token);
        localStorage.setItem('refreshToken', res.tokens.refresh.token);
        localStorage.setItem('userInfo', JSON.stringify(res.user));
        // notificationToast('success', 'signup-success');
        navigate('/');
        return res;
      }
    } catch (error) {
      console.log('error', error);
      reRef.current.reset();
      setButtonDisabled(true);
      notificationToast('error', error.response.data !== undefined ? error.response.data.message : 'SERVER_ERROR');
    }
  };
  const onChange = (value) => {
    if (value) {
      setButtonDisabled(false);
    }
  };
  return (
    <div className={cx('wrapper')} style={{ backgroundImage: `url(${images.loginBackground})` }}>
      <div className={cx('form-wrapper')}>
        <div className={cx('login-title')}>Chào mừng đến với Bình nguyên vô tận</div>
        <Form
          name="normal_login"
          className={cx('login-form')}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="email"
            rules={[
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Vui lòng nhập tài khoản của bạn!',
              },
            ]}
          >
            <Input prefix={<UserOutlined className={cx('site-form-item-icon')} />} placeholder="Username" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập mật khẩu của bạn!',
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className={cx('site-form-item-icon')} />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>

          <Form.Item name="captcha">
            <ReCAPTCHA ref={reRef} sitekey={process.env.REACT_APP_GOOGLE_SITE_KEY_V2} onChange={onChange} />
          </Form.Item>

          <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>Nhớ mật khẩu</Checkbox>
            </Form.Item>

            <a className={cx('login-form-forgot')} href="/forgot-password">
              Quên mật khẩu
            </a>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" className={cx('login-form-button')} disabled={buttonDisabled}>
              Đăng nhập
            </Button>
            <div style={{ paddingTop: '1rem' }}>
              Bạn chưa có tài khoản ?{' '}
              <Link className={cx('login-form-register-text')} to="/register">
                Đăng ký ngay !
              </Link>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default LoginPage;
