import React from 'react';
import { Divider, Form, Input, Button } from 'antd';
import styles from './Profile.module.scss';
import classNames from 'classnames/bind';
import { changePassWord } from '~/services/user.service';
import notificationToast from '~/utils/toast';
import { useNavigate } from 'react-router-dom';
const cx = classNames.bind(styles);
const Profile = () => {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const userInfo = JSON.parse(localStorage.getItem('userInfo'));
  const onFinish = async (value) => {
    let dataChange = {
      newPassword: value.newPassword,
      oldPassword: value.oldPassword,
    };
    let res = await changePassWord(userInfo.id, dataChange);
    if (res.code !== 'ERR_BAD_REQUEST') {
      notificationToast('success', 'thay đổi thành công');
      localStorage.clear();
      navigate('/login');
    } else {
      notificationToast('error', res.code);
    }
  };

  return (
    <div className={cx('wrapper')}>
      <div className={cx('title')}>Change Password</div>
      <Divider />
      <Form form={form} name="changePassword" onFinish={onFinish} scrollToFirstError labelAlign="left">
        <Form.Item
          name="oldPassword"
          label="Old PassWord"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
            {
              pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
              message: 'Mật khẩu sai format',
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="newPassword"
          label="New PassWord"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
            {
              pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
              message: 'Mật khẩu sai format',
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="confirm"
          label="Confirm New Password"
          dependencies={['newPassword']}
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please confirm your password!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('newPassword') === value) {
                  return Promise.resolve();
                }

                return Promise.reject(new Error('The two passwords that you entered do not match!'));
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item>
          <Button className={cx('button-Submit')} htmlType="submit">
            Confirm Update PassWord
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Profile;
