import React, { useState } from 'react';
import { Button, Checkbox, Form, Input, Select, Cascader } from 'antd';
import styles from './Register.module.scss';
import classNames from 'classnames/bind';
import images from '~/assets/images/index';
import { Link } from 'react-router-dom';
import { signUpMe } from '~/services/user.service';
import notificationToast from '~/utils/toast';
import { useNavigate } from 'react-router-dom';
const { Option } = Select;
const cx = classNames.bind(styles);

const residences = [
  {
    value: 'Hà Nội',
    label: 'Hà Nội',
    children: [
      {
        value: 'Ứng Hòa',
        label: 'Ứng Hòa',
        children: [
          {
            value: 'Đồng Tân',
            label: 'Đồng Tân',
          },
          {
            value: 'Cầu Lão',
            label: 'Cầu Lão',
          },
        ],
      },
      {
        value: 'Mỹ Đức',
        label: 'Mỹ Đức',
        children: [
          {
            value: 'Lê Thanh',
            label: 'Lê Thanh',
          },
          {
            value: 'Hồng Sơn',
            label: 'Hồng Sơn',
          },
        ],
      },
    ],
  },
];

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 24,
      offset: 0,
    },
  },
};

const RegisterPage = () => {
  const [form] = Form.useForm();
  let navigate = useNavigate();
  const onFinish = async (values) => {
    const dataBody = {
      name: values.name,
      email: values.email,
      password: values.password,
      phoneNumber: values.phoneNumber,
    };
    try {
      const res = await signUpMe(dataBody);
      console.log('Respone', res);
      if (res.status === 201) {
        localStorage.setItem('token', res.data.tokens.access.token);
        localStorage.setItem('refreshToken', res.data.tokens.refresh.token);
        localStorage.setItem('userInfo', JSON.stringify(res.data.user));
        notificationToast('success', 'signup-success');
        navigate('/');
        return res;
      }
    } catch (error) {
      return notificationToast('error', error.response.data.message);
    }
  };

  return (
    <div className={cx('wrapper')} style={{ backgroundImage: `url(${images.loginBackground})` }}>
      <div className={cx('form-wrapper')}>
        <div className={cx('register-title')}>Chào mừng đến với Bình nguyên vô tận</div>
        <Form
          {...formItemLayout}
          form={form}
          name="register"
          onFinish={onFinish}
          initialValues={{
            residence: ['Hà Nội', 'Mỹ Đức', 'Lê Thanh'],
            prefix: '84',
          }}
          scrollToFirstError
          labelAlign="left"
        >
          <Form.Item
            name="name"
            label="Name"
            rules={[
              {
                required: true,
                message: 'Please input your name',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="email"
            label="E-mail"
            rules={[
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="phoneNumber"
            label="Phone Number"
            rules={[
              {
                required: true,
                message: 'Please input your phone number!',
              },
              {
                pattern: /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,
                message: 'Vui lòng nhập đúng',
              },
            ]}
          >
            <Input
              style={{
                width: '100%',
              }}
            />
          </Form.Item>

          <Form.Item
            name="residence"
            label="Habitual Residence"
            rules={[
              {
                type: 'array',
                required: true,
                message: 'Please select your habitual residence!',
              },
            ]}
          >
            <Cascader options={residences} />
          </Form.Item>

          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
              {
                pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
                message: 'Mật khẩu sai format',
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(new Error('The two passwords that you entered do not match!'));
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="agreement"
            valuePropName="checked"
            rules={[
              {
                validator: (_, value) =>
                  value ? Promise.resolve() : Promise.reject(new Error('Bạn phải chấp nhận điều khoản và chính sách ')),
              },
            ]}
            {...tailFormItemLayout}
          >
            <Checkbox>
              Tôi đồng ý với <Link to="#">Điều khoản dịch vụ & Chính sách quyền riêng tư</Link>
            </Checkbox>
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button className={cx('register-form-button')} type="primary" htmlType="submit">
              Đăng ký
            </Button>
            <div style={{ paddingTop: '1rem' }}>
              Bạn đã tài khoản ?{' '}
              <Link className={cx('register-form-login-text')} to="/login">
                Đăng nhập !
              </Link>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default RegisterPage;
