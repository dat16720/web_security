import React, { useState, useEffect, useRef } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input } from 'antd';
import styles from '../Register/Register.module.scss';
import { useNavigate, useParams } from 'react-router-dom';
import classNames from 'classnames/bind';
import images from '~/assets/images';
import { resetPass } from '~/services/user.service';
import notificationToast from '~/utils/toast';

const cx = classNames.bind(styles);

const ResetPassword = () => {
  const navigate = useNavigate();
  const reRef = useRef();
  const { token } = useParams();

  const onFinish = async (values) => {
    let data = {
      password: values.password,
    };
    let resData = await resetPass({ token }, data);
    if (resData?.code === 'ERR_BAD_REQUEST') {
      notificationToast('error', resData?.response?.statusText);
    } else {
      navigate('/login');
    }
  };
  return (
    <div className={cx('wrapper')} style={{ backgroundImage: `url(${images.loginBackground})` }}>
      <div className={cx('form-wrapper')}>
        <div className={cx('register-title')}>Nhập mật khẩu mới</div>
        <Form
          name="normal_login"
          className={cx('login-form')}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
              {
                pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
                message: 'Mật khẩu sai format',
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(new Error('The two passwords that you entered do not match!'));
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" className={cx('login-form-button')}>
              Reset Password
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default ResetPassword;
