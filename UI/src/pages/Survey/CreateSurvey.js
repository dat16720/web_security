import React, { useState, useEffect } from 'react';
import classNames from 'classnames/bind';
import styles from './Survey.module.scss';
import { Form, Input, Button, Space, Select } from 'antd';
import { PlusCircleOutlined, PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import { CreateSurveyoke } from '~/services/survey.service';
import notificationToast from '~/utils/toast';
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};
const cx = classNames.bind(styles);
const CreateSurvey = () => {
  const [form] = Form.useForm();
  //   const [questionNumder, setQuestionNumber] = useState([1]);

  const sights = {
    Beijing: ['Tiananmen', 'Great Wall'],
    Shanghai: ['Oriental Pearl', 'The Bund'],
  };
  const userInfo = JSON.parse(localStorage.getItem('userInfo'))
  const onFinish = async (values) => {
    let dataBody = {...values, createdById: userInfo.id}
    // console.log("databody", dataBody, userInfo)
    let dataRes = await CreateSurveyoke(dataBody);
    if (!dataRes.code) {
      notificationToast('success', 'Tạo thành công')
    }
    else {
      notificationToast('error', dataRes?.code)
    }
  };
  const onReset = () => {
    form.current.resetFields();
  };
  const onFill = () => {
    form.current.setFieldsValue({
      note: 'Hello world!',
      gender: 'male',
    });
  };
  //   const onAddQuestion = () => {
  //     setQuestionNumber([...questionNumder, questionNumder.length + 1]);
  //   };
  //   const hadnleDelete = (item) => {
  //     let newNumberQuestion = [...questionNumder];
  //     newNumberQuestion.splice(item, 1);
  //     setQuestionNumber(newNumberQuestion);
  //   };

  return (
    <div className={cx('wrapper')}>
      <h2>Tạo khảo sát</h2>
      <Form
        {...layout}
        ref={form}
        name="dynamic_form_nest_item"
        onFinish={onFinish}
        labelAlign="left"
        scrollToFirstError
      >
        <Form.Item
          name="title"
          label="Tiêu đề"
          rules={[
            {
              required: true,
              message: 'Nhập tiêu dề khảo sát',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="description"
          label="Mô tả"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.List name="questions">
          {(fields, { add, remove }) => (
            <>
              {fields.map((field) => (
                <div key={field.key} style={{ display: 'flex' }}>
                  <Form.Item
                    {...field}
                    label="Câu hỏi"
                    name={[field.name, 'question']}
                    key={field.key}
                    rules={[
                      {
                        required: true,
                        message: 'Missing question',
                      },
                    ]}
                    style={{ flex: '1', marginLeft: '1%' }}
                  >
                    <Input style={{ width: '97%' }} />
                  </Form.Item>

                  <MinusCircleOutlined onClick={() => remove(field.name)} style={{ paddingTop: '7px' }} />
                </div>
              ))}

              <Form.Item className={cx('add_question')}>
                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />} danger>
                  Thêm câu hỏi
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>
        {/* {questionNumder.map((item, key) => {
          return (
            <Form.Item
              name={`questrion${item}`}
              label={`Câu hỏi kháo sát số ${item}`}
              rules={[
                {
                  required: true,
                  //   message: 'Nhập câu hỏi kháo sát số ${item}',
                },
              ]}
            >
              <Input />
              <Button type="primary" onClick={() => hadnleDelete(item - 1)}>
                Xóa
              </Button>
            </Form.Item>
          );
        })} */}

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
          <Button htmlType="button" onClick={onReset}>
            Reset
          </Button>
          {/* <Button type="primary" htmlType="button" onClick={onAddQuestion}>
            Thêm câu hỏi
          </Button> */}
          <Button type="link" htmlType="button" onClick={onFill}>
            Fill form
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default CreateSurvey;
