import React, { useState, useEffect } from 'react';
import { LikeOutlined, MessageOutlined, StarOutlined } from '@ant-design/icons';
import { Avatar, List, Space, Button, Form, Input } from 'antd';
import classNames from 'classnames/bind';
import styles from '../Survey.module.scss';
import { useParams } from 'react-router-dom';
import { getSurvey } from '~/services/survey.service';
import avataDefault from '~/assets/images/index'
const cx = classNames.bind(styles);

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

const DetailSurvey = () => {
  let {surveyId} = useParams();

  const [form] = Form.useForm();
  const [detailSurveyId, setDetailSurveyId] = useState({
    surveyId: 1,
    title: 'Tiêu đề khao sat mẫu 1',
    description: 'Mô tả khảo sát 1',
    status: 'phê duyệt',
    questions: [
      {
        question: 'cÂU HỎI KS số 1',
      },
      {
        question: 'cÂU HỎI KS  số 2',
      },
    ],
  });
  const getSurveyoke = async () => {
    let res = await getSurvey(surveyId);
    setDetailSurveyId(res);
    console.log("ress",res)
    // return setListSurvey(res?.results);
  };
  useEffect(() => {
    getSurveyoke();
  }, []);
  const IconText = ({ icon, text }) => (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  );
  const onFinish = (values) => {
    console.log("valuse",values);
  };
  return (
    <div className={cx('wrapper')}>
      <h2>Chi tiết khảo sát</h2>
      <List itemLayout="vertical" size="large">
        <List.Item
          key={detailSurveyId.title}
          actions={[
            <IconText icon={StarOutlined} text="156" key="list-vertical-star-o" />,
            <IconText icon={LikeOutlined} text="156" key="list-vertical-like-o" />,
            <IconText icon={MessageOutlined} text="2" key="list-vertical-message" />,
          ]}
          extra={
            <img width={272} alt="logo" src={avataDefault.loginBackground} />
          }
          //   onClick={() => {<Link to={item.id}> </Link>}}
        >
          <List.Item.Meta
            avatar={<Avatar src={detailSurveyId.avatar || avataDefault.loginBackground} />}
            title={detailSurveyId.title}
            description={detailSurveyId.description}
          />

          <Form {...layout} form={form} onFinish={onFinish} name="nest-messages">
            {detailSurveyId.questions.map((lsquestion) => {
              return (
                <Form.Item
                  name={lsquestion.question}
                  label={lsquestion.question}
                  rules={[
                    {
                      required: true,
                      message: 'Nhập câu trả lời cẩn thận vào',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              );
            })}
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </List.Item>
      </List>
    </div>
  );
};

export default DetailSurvey;
