import React, { useState, useEffect } from 'react';
import { LikeOutlined, MessageOutlined, StarOutlined } from '@ant-design/icons';
import { Avatar, List, Space, Button, Radio } from 'antd';
import classNames from 'classnames/bind';
import styles from './Survey.module.scss';
import { Link } from 'react-router-dom';
import { deleteSurvey, getSurveys, updateSurvey } from '~/services/survey.service';
import avataDefault from '~/assets/images/index';
import notificationToast from '~/utils/toast';
const cx = classNames.bind(styles);
// const listSurvey = [
//   {
//     surveyId: 1,
//     title: 'Tiêu đề khao sat mẫu 1',
//     description: 'Mô tả khảo sát 1',
//     status: 'phê duyệt',
//     questions: [
//       {
//         question: 'Khảo sát số 1',
//       },
//       {
//         question: 'Khảo sát số 2',
//       },
//     ],
//   },
//   {
//     surveyId: 2,
//     title: 'Tiêu đề khao sat mẫu 2',
//     description: 'Mô tả khảo sát 2',
//     status: 'đợi chờ',
//     questions: [
//       {
//         question: 'Khảo sát số 1',
//       },
//       {
//         question: 'Khảo sát số 2',
//       },
//     ],
//   },
//   {
//     surveyId: 3,
//     title: 'Tiêu đề khao sat mẫu 3',
//     description: 'Mô tả khảo sát 3',
//     status: 'đợi chờ',
//     questions: [
//       {
//         question: 'Khảo sát số 1',
//       },
//       {
//         question: 'Khảo sát số 2',
//       },
//     ],
//   },
// ];
const IconText = ({ icon, text }) => (
  <Space>
    {React.createElement(icon)}
    {text}
  </Space>
);
const Survey = () => {
  // const userInfo = JSON.parse(localStorage.getItem('userInfo'))
  const userInfo = JSON.parse(localStorage.getItem('userInfo'));
  const [listSurvey, setListSurvey] = useState([]);
  const [loading, setLoading] = useState(false);
  const handleApprove = async (id) => {
    let resData = await updateSurvey(id, { status: 'APPROVED' });
    if (resData.response.status === 403) {
      notificationToast('error', resData.message);
    }
    setLoading(true);
  };
  const handleRejected = async (id) => {
    let resData = await updateSurvey(id, { status: 'REJECTED' });
    if (resData.response.status === 403) {
      notificationToast('error', resData.message);
    }
    setLoading(true);
  };
  const handleDelete = async (id) => {
    let resData = await deleteSurvey(id);
    if (resData.response.status === 403) {
      notificationToast('error', resData.message);
    }
    setLoading(true);
  };
  const [params, setParams] = useState({});
  const getListSurvey = async () => {
    console.log('params', params);
    let res = await getSurveys(params);
    setLoading(false);
    return setListSurvey(res?.results);
    // let params = userInfo.role === 'user' ? { status: 'APPROVED' } : {};

    // if (userInfo.role === 'user') {
    //   setParams({ status: 'APPROVED' });
    //   let res = await getSurveys(params);
    //   setLoading(false);
    //   return setListSurvey(res?.results);
    // } else if (userInfo.role === 'admin') {
    //   let res = await getSurveys(params);
    //   setLoading(false);
    //   return setListSurvey(res?.results);
    // } else {
    //   return;
    // }
  };
  useEffect(() => {
    getListSurvey();
  }, [loading, params]);

  // const [value, setValue] = useState(1);

  const onChange = (e) => {
    // console.log('radio checked', e.target.value);
    setParams({ status: e.target.value });
  };
  return (
    <div className={cx('wrapper')}>
      <h2>Danh sách khảo sát</h2>
      <div className="filter_survey">
        <Radio.Group onChange={onChange} value={params}>
          <Radio value="APPROVED">Đã phê duyệt</Radio>
          <Radio value="REJECTED">Đã từ chối</Radio>
          <Radio value="WAITING_TO_REVIEW">Đợi duyệt</Radio>
        </Radio.Group>
      </div>
      <div className={cx('list-survey')}>
        <List
          itemLayout="vertical"
          size="large"
          pagination={{
            onChange: (page) => {
              console.log(page);
            },
            pageSize: 5,
          }}
          dataSource={listSurvey}
          //   footer={
          //     <div>
          //       <b>ant design</b> footer part
          //     </div>
          //   }
          renderItem={(item) => (
            <List.Item
              key={item.title}
              // actions={[
              //   <IconText icon={StarOutlined} text="156" key="list-vertical-star-o" />,
              //   <IconText icon={LikeOutlined} text="156" key="list-vertical-like-o" />,
              //   <IconText icon={MessageOutlined} text="2" key="list-vertical-message" />,
              // ]}
              // extra={
              //   <img
              //     width={272}
              //     alt="logo"
              //     src="https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png"
              //   />
              // }
              //   onClick={() => {<Link to={item.id}> </Link>}}
            >
              <List.Item.Meta
                avatar={<Avatar src={item.avatar || avataDefault.loginBackground} />}
                title={<Link to={item.id}>{item.title}</Link>}
                // description={item.description}
              />
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div>Mô tả: {item.description}</div>
                <div
                  style={{
                    color:
                      item.status === 'WAITING_TO_REVIEW'
                        ? '#2c3e50'
                        : item.status === 'REJECTED'
                        ? '#e74c3c'
                        : '#27ae60',
                  }}
                >
                  Trạng thái:{' '}
                  {item.status === 'WAITING_TO_REVIEW'
                    ? 'Đợi duyệt'
                    : item.status === 'REJECTED'
                    ? 'Từ chối'
                    : 'Đã duyệt'}
                </div>
                {/* status for admin */}
                <div className={cx('status-survey')}>
                  <Button className={cx('approve')} onClick={() => handleApprove(item.id)}>
                    Phê duyệt
                  </Button>
                  <Button className={cx('reject')} onClick={() => handleRejected(item.id)}>
                    Từ chối
                  </Button>
                  <Button className={cx('delete')} onClick={() => handleDelete(item.id)}>
                    Xóa
                  </Button>
                </div>
              </div>
            </List.Item>
          )}
        />
      </div>
    </div>
  );
};

export default Survey;
