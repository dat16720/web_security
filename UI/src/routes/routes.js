import config from '~/config';

// Layouts
import { HeaderOnly, NoAuthLayout } from '~/layouts';

// Pages
import Home from '~/pages/Home';
import Survey from '~/pages/Survey';
import Profile from '~/pages/Profile';
import Upload from '~/pages/Upload';
import Search from '~/pages/Search';
import Live from '~/pages/Live';
import LoginPage from '~/pages/Login';
import RegisterPage from '~/pages/Register';
import ForgotPassword from '~/pages/ForgotPass';
import DetailSurvey from '~/pages/Survey/DetailSurvey/DetailSurvey';
import CreateSurvey from '~/pages/Survey/CreateSurvey';
import ResetPassword from '~/pages/ResetPass';
// Public routes
const publicRoutes = [
  { path: config.routes.home, component: Home },

  { path: config.routes.live, component: Live },
  { path: config.routes.profile, component: Profile },
  { path: config.routes.upload, component: Upload, layout: HeaderOnly },
  { path: config.routes.search, component: Search, layout: null },
  { path: config.routes.login, component: LoginPage, layout: NoAuthLayout },
  { path: config.routes.register, component: RegisterPage, layout: NoAuthLayout },
  { path: config.routes.forgotpass, component: ForgotPassword, layout: NoAuthLayout },
  { path: config.routes.resetpass, component: ResetPassword, layout: NoAuthLayout },
  //////
  /// Survey
  { path: config.routes.survey, component: Survey },
  { path: config.routes.surveyId, component: DetailSurvey },
  { path: config.routes.createSurvey, component: CreateSurvey },
];

const privateRoutes = [];

export { publicRoutes, privateRoutes };
