import SurveyModelService from './surveyModel';
import userProfileService from './userProfile';

const apiFactory = {
  user: new userProfileService(),
  survey: new SurveyModelService(),
};
export default apiFactory;
