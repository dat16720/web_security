import BaseModel from '../../axiosConfig';
import { CREATE_SURVEY, GET_SURVEY, LOGOUT_USER, PROFILE_ME, PROFILE_USER } from '../../apiConstant';

export default class SurveyModelService extends BaseModel {
  getListSurveys(params) {
    return this.apiGet(CREATE_SURVEY, {params});
  }
  // getSurveyById(surveyId) {
  //   console.log("surveyId",surveyId)
  //   return this.apiGet(`http://localhost:1607/v1/survey/${surveyId}`, {});
  // }
  getSurveyById(surveyId, param = {}) {
    const url = `${GET_SURVEY}/${surveyId}`;
    return this.apiGet(url, { param });
  }
  createSurvey(data = {}) {
    return this.apiPost(CREATE_SURVEY, { data });
  }
  deleteSurveyById(surveyId) {
    return this.apiDelete(`${GET_SURVEY}/${surveyId}`, {});
  }
  updateSurveyById(surveyId, data) {
    return this.apiPatch(`${GET_SURVEY}/${surveyId}`, { data });
  }
  //   getProfileMe(param = {}) {
  //     return this.apiGet(PROFILE_ME, { param })
  //   }
  //   getProfileUserById(id, param = {}) {
  //     const url = `${PROFILE_USER}/${id}`
  //     return this.apiGet(url, { param })
  //   }
  //   updateProfile(data = {}) {
  //     const url = `${PROFILE_USER}`
  //     return this.apiPatch(url, { data })
  //   }
  //   logOutCurrentUser(data) {
  //     return this.apiDelete(LOGOUT_USER, { data })
  //   }
}
