import BaseModel from '../../axiosConfig';
import { CHANGE_PASS, LIST_USER, LOGOUT_USER, PROFILE_ME, PROFILE_USER, FORGOT_PASSWORD, RESET_PASSWORD } from '../../apiConstant';

export default class userProfileService extends BaseModel {

  forgotPass(data = {}) {
    return this.apiPost(FORGOT_PASSWORD, { data });
  }
  resetPassById(params= {}, data ={}){
    return this.apiPost(RESET_PASSWORD, { params, data });
  }

  getProfileMe(param = {}) {
    return this.apiGet(PROFILE_ME, { param });
  }
  getProfileUserById(id, param = {}) {
    const url = `${PROFILE_USER}/${id}`;
    return this.apiGet(url, { param });
  }
  updateProfile(data = {}) {
    const url = `${PROFILE_USER}`;
    return this.apiPatch(url, { data });
  }
  logOutCurrentUser(data = {}) {
  
    return this.apiPost(LOGOUT_USER, { data });
  }

  getListUsers(params) {
    return this.apiGet(LIST_USER, { params });
  }

  deleteUserorEmployee(userId){
    console.log("iod",userId )
    return this.apiDelete(`${LIST_USER}/${userId}`, {})
  }
  //
  changePass(userId, data = {}) {
    return this.apiPost(`${CHANGE_PASS}/${userId}`, { data });
  }
}
