// import { HOST_URL } from "urlConfig";

// export const API_DOMAIN = process.env.REACT_APP_API_DOMAIN
export const API_DOMAIN = 'http://localhost:1607';
export const API_DOMAIN_CHAT = 'https://chat.alo389.vn';
export const API_BASE = API_DOMAIN + '/v1';

export const GET_ME = `${API_BASE}/auth/login`;
export const SIGN_UP_ME = `${API_BASE}/auth/register`;
export const REFRESH_TOKEN = `/auth/refresh-tokens`;
export const FORGOT_PASSWORD = `/auth/forgot-password`;
export const RESET_PASSWORD = `/auth/reset-password`;
export const LOGIN_WITH_GOOGLE = `/auth/login-google`;
export const LOGIN_NORMAL = `/auth/login`;
export const LOGIN_WITH_FACEBOOK = `/auth/login-facebook`;
export const LOGOUT_USER = `/auth/logout`;

//
export const PROFILE_ME = `${API_BASE}/user/info`;
export const PROFILE_USER = `${API_BASE}/user`;

export const LIST_USER = `${API_BASE}/users`;

export const CHANGE_PASS = `${API_BASE}/users/change-password`;

// survey
export const CREATE_SURVEY = `${API_BASE}/survey/create-survey`;
// export const GET_SURVEYS = `${API_BASE}/survey`
export const GET_SURVEY = `${API_BASE}/survey`;
