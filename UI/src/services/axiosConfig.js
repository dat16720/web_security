import axios from 'axios'
import { API_BASE, REFRESH_TOKEN } from './apiConstant'
import qs from 'qs'
import history from '../utils/history'

export const requestToken = async () => {
  try {
    const refreshTokenLocal = localStorage.getItem('refreshToken')
    const data = {
      refreshToken: `Bearer ${refreshTokenLocal}`
    }
    const res = await axios({
      baseURL: API_BASE,
      url: REFRESH_TOKEN,
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      data
    })
    localStorage.setItem('token', res.data.token)
    localStorage.setItem('refreshToken', res.data.refreshToken)
    return res
  } catch (e) {
    localStorage.clear()
    history.push('/login')
    return Promise.reject(e)
  }
}
const $axios = axios.create({
  baseURL: API_BASE,
  timeout: 50000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json'
  }
})

$axios.interceptors.request.use(
  function (config) {
    let accessToken = localStorage.getItem('token')
    if (accessToken) {
      config.headers.common['Authorization'] = 'Bearer ' + accessToken
    }
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

$axios.interceptors.response.use(
  response => response,
  async error => {
    const errorResponse = error.response
    const prevRequest = error?.config
    if (errorResponse && errorResponse.status === 401) {
 
      if (localStorage.getItem('refreshToken')) {
        try {
          const res = await requestToken()
          prevRequest.headers['Authorization'] = `Bearer ${res.data.token}`
          localStorage.setItem('token', res.data.token)
          localStorage.setItem('refreshToken', res.data.refreshToken)
          return $axios(prevRequest)
        } catch (error) {
          console.log(error)
          return Promise.reject(error)
        }
      } else {
        localStorage.clear()
        history.push('/login')
      }
    }
    return Promise.reject(error)
  }
)

export default class BaseModel {
  apiGet(url, { params = {}, otps = {}, qsOtps = {}, baseURL = API_BASE }) {
 
    return $axios.get(
      url,
      {
        baseURL,
        params,
        paramsSerializer: param =>
          qs.stringify(param, {
            arrayFormat: 'repeat',
            charset: 'utf-8',
            ...qsOtps
          })
      },
      otps
    )
  }

  apiPost(
    url,
    { params = {}, data = {}, otps = {}, qsOtps = {}, baseURL = API_BASE }
  ) {
    const paramString = qs.stringify(params, {
      arrayFormat: 'repeat',
      charset: 'utf-8',
      ...qsOtps
    })
    const urlPost = paramString ? `${url}?${paramString}` : url
    if (baseURL !== API_BASE) {
      return $axios({ url: urlPost, baseURL, data, method: 'post' })
    }
    return $axios.post(urlPost, data, otps)
  }

  apiPut(
    url,
    { params = {}, data = {}, otps = {}, qsOtps = {}, baseURL = API_BASE }
  ) {
    const paramString = qs.stringify(params, {
      arrayFormat: 'repeat',
      charset: 'utf-8',
      ...qsOtps
    })
    const urlPut = paramString ? `${url}?${paramString}` : url
    if (baseURL !== API_BASE) {
      return $axios({ url: urlPut, baseURL, data, method: 'put' })
    }
    return $axios.put(urlPut, data, otps)
  }

  apiPatch(
    url,
    { params = {}, data = {}, otps = {}, qsOtps = {}, baseURL = API_BASE }
  ) {
    const paramString = qs.stringify(params, {
      arrayFormat: 'repeat',
      charset: 'utf-8',
      ...qsOtps
    })

    const urlPut = paramString ? `${url}?${paramString}` : url
    if (baseURL !== API_BASE) {
      return $axios({ url: urlPut, baseURL, data, method: 'patch' })
    }
    return $axios.patch(urlPut, data, otps)
  }

  apiDelete(
    url,
    { params = {}, otps = {}, qsOtps = {}, data = {}, baseURL = API_BASE }
  ) {
    const paramString = qs.stringify(params, {
      arrayFormat: 'repeat',
      charset: 'utf-8',
      ...qsOtps
    })
    const urlDelete = paramString ? `${url}?${paramString}` : url
    if (baseURL !== API_BASE) {
      return $axios({ url: urlDelete, baseURL, data, method: 'delete' })
    }
    return $axios.delete(urlDelete, data, otps)
  }
}
