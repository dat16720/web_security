import apiFactory from './api/model';
export const getSurveys = async (params) => {
  try {
    const res = await apiFactory.survey.getListSurveys(params);
    const surveys = await res.data;
    return surveys;
  } catch (e) {
    console.log('errro', e);
    return e;
  }
};
export const getSurvey = async (surveyId) => {
  try {
    const res = await apiFactory.survey.getSurveyById(surveyId);
    const survey = await res.data;
    return survey;
  } catch (e) {
    console.log('errro', e);
    return e;
  }
};
export const CreateSurveyoke = async (data) => {
  try {
    const res = await apiFactory.survey.createSurvey(data);

    const survey = await res.data;
    return survey;
  } catch (e) {
    console.log("errro",e);
    return e;
  }
};

export const deleteSurvey = async (surveyId) => {
  try {
    const res = await apiFactory.survey.deleteSurveyById(surveyId);
    const survey = await res.data;
    return survey;
  } catch (e) {
    console.log('errro', e);
    return e;
  }
};

export const updateSurvey = async (surveyId, data) => {
  try {
    const res = await apiFactory.survey.updateSurveyById(surveyId, data);
    const survey = await res.data;
    return survey;
  } catch (e) {
    console.log('errro', e);
    return e;
  }
};