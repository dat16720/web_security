import axios from 'axios';
import {
  API_BASE,
  FORGOT_PASSWORD,
  GET_ME,
  LOGIN_WITH_FACEBOOK,
  LOGIN_WITH_GOOGLE,
  LOGIN_NORMAL,
  RESET_PASSWORD,
  SIGN_UP_ME,
} from './apiConstant';
import apiFactory from '../services/api/model';

export const getMe = async (body) => {
  const res = await axios({
    method: 'post',
    url: `${GET_ME}`,
    data: body,
  });
  if (res.response) {
    return res.response;
  }
  return res;
};
export const signUpMe = async (body) => {
  const res = await axios({
    method: 'post',
    url: `${SIGN_UP_ME}`,
    data: body,
  });

  if (res.response) {
    return res.response;
  }
  return res;
};

export const forgetPassword = async (data) => {
  const res = await axios({
    method: 'post',
    baseURL: API_BASE,
    url: FORGOT_PASSWORD,
    data,
  });
  if (res.response) {
    return res.response;
  }
  return res;
};

export const resetPassword = async (data) => {
  const res = await axios({
    method: 'patch',
    baseURL: API_BASE,
    url: RESET_PASSWORD,
    data,
  });
  if (res.data) {
    return res.data;
  }
  return res;
};

export const loginNormal = async (data) => {
  const res = await axios({
    method: 'post',
    baseURL: API_BASE,
    url: LOGIN_NORMAL,
    data,
  });
  if (res.data) {
    return res.data;
  }
  return res;
};

export const loginWithGoogle = async (data) => {
  const res = await axios({
    method: 'post',
    baseURL: API_BASE,
    url: LOGIN_WITH_GOOGLE,
    data,
  });
  if (res.data) {
    return res.data;
  }
  return res;
};

export const getProfileUserById = async (userId) => {
  try {
    const res = await apiFactory.user.getProfileUserById(userId);
    const user = await res.data;
    return user;
  } catch (e) {
    console.log(e);
  }
};

export const loginWithFacebook = async (data) => {
  const res = await axios({
    method: 'post',
    baseURL: API_BASE,
    url: LOGIN_WITH_FACEBOOK,
    data,
  });
  if (res.data) {
    return res.data;
  }
  return res;
};
export const LogOutUser = async (data) => {
  try {
    const res = await apiFactory.user.logOutCurrentUser(data);
    return res;
  } catch (error) {
    return error;
  }
};
export const updateProfile = async (data) => {
  try {
    const res = await apiFactory.user.updateProfile(data);
    return res;
  } catch (error) {
    console.log(error);
  }
};

export const getUsers = async (params) => {
  try {
    const res = await apiFactory.user.getListUsers(params);

    const users = await res.data;
    return users;
  } catch (e) {
    console.log(e);
    return e;
  }
};

export const changePassWord = async (userId, data) => {
  try {
    const res = await apiFactory.user.changePass(userId, data);

    const users = await res.data;
    return users;
  } catch (e) {
    console.log(e);
    return e;
  }
};

export const deteleUser = async (userId) => {
  try {
    const res = await apiFactory.user.deleteUserorEmployee(userId);

    const users = await res.data;
    return users;
  } catch (e) {
    console.log(e);
    return e;
  }
};

export const forgotPass = async (token) => {
  try {
    const res = await apiFactory.user.forgotPass(token);

    const users = await res.data;
    return users;
  } catch (e) {
    console.log(e);
    return e;
  }
};
export const resetPass = async (params, data) => {

  try {
    const res = await apiFactory.user.resetPassById(params, data);

    const users = await res.data;
    return users;
  } catch (e) {
    return e;
  }
};
