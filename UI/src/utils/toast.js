import { notification } from 'antd'

const notificationToast = (type, message = '') => {
  notification[type]({
    message: type,
    description: message
  })
}

export default notificationToast
